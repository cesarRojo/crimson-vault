﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyTrigger : MonoBehaviour
{
    public GameManager enemyManager;

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                enemyManager.LostLevel();
            }
        }
}
