﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Toggle_Changed(bool newValue)
    {

    }

     public AudioMixer audioMixer;

     public void Change_Volume(float volume)
     {
         audioMixer.SetFloat("volume", volume);
     }
}
