﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public GameObject completeLevelUI;
    public GameObject lostLevelUI;
    private int nextSceneToLoad;
    private int restartScene;

    private void Start()
    {

        nextSceneToLoad = SceneManager.GetActiveScene().buildIndex + 1;

        restartScene = SceneManager.GetActiveScene().buildIndex;
    }

    //Elementos que se esperan 4 seg
    public void LostLevel()
    {
        lostLevelUI.SetActive(true);
        Invoke("Reset", 3);
    }

    public void CompleteLevel()
    {
        completeLevelUI.SetActive(true);
        Invoke("Win", 4);
    }


   //Funciones para reiniciar las escenas
    public void Win()
    {
        
            SceneManager.LoadScene(nextSceneToLoad);
         
        
    }
    private void Reset()
    {
        SceneManager.LoadScene(restartScene);
    }
      
}
