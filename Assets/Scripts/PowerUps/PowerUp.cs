﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{

    public GameObject pickupEffect;
    public AudioClip collectSound;
    public float volume = 0.5f;

    void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Pickup();
            
        }
    }

    void Pickup() 
    {
        //Sound effect
        AudioSource.PlayClipAtPoint(collectSound, transform.position, volume);
        

        //Spawn cool effect
        Instantiate(pickupEffect, transform.position, transform.rotation);

        //Apply powerup to player
        KeepScore.Score +=100;

        Destroy(gameObject);
    }
}
