﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonSFX : MonoBehaviour
{
    public AudioSource SFX;
    public AudioClip hoverSFX;
    public AudioClip clickSFX;

    public void HoverSound()
    {
        SFX.PlayOneShot(hoverSFX);
    }

    public void ClickSound()
    {
        SFX.PlayOneShot(clickSFX);
    }
}
