﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class wheelrotate : MonoBehaviour {
 
    public WheelCollider FL;
    public WheelCollider FR;
    public WheelCollider RL;
    public WheelCollider RR;
 
    public Transform wheelFLTrans;
    public Transform wheelFRTrans;
    public Transform wheelBLTrans;
    public Transform wheelBRTrans;

    public int maxMotorTorque = 1000;
    public int maxSteeringAngle = 100;
    public float torque;
    public float SteerAngle;
 
    void Update ()
 
    {
        wheelFLTrans.Rotate (0, FL.rpm / 60 * 360 * Time.deltaTime, 0);
        wheelFRTrans.Rotate (0, FR.rpm / 60 * 360 * Time.deltaTime, 0);
        wheelBLTrans.Rotate (0, RL.rpm / 60 * 360 * Time.deltaTime, 0);
        wheelBRTrans.Rotate (0, RR.rpm / 60 * 360 * Time.deltaTime, 0);
    }
 
}