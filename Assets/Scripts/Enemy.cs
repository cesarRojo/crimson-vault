﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public Transform target;
    public float speed;
    private float spp = 0;
    int num = 0;
    Rigidbody rig;

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody>();
    }
    void reg()
    {
        num = 12;
    }
    void reg2()
    {
        num = 13;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 pos = Vector3.MoveTowards(transform.position, target.position, speed * Time.fixedDeltaTime);
        rig.MovePosition(pos);
        transform.LookAt(target);

        if (speed <= 1.2)
        {
            spp = spp + 1f;
            if (spp % 150 == 0)
            {
                speed = speed + 1f;
            }
        }else if (speed >= 1.2 && num<=11)
        {
            speed = 1.1f;
            Invoke("reg", 7);
            Invoke("reg2", 20);
        }
        else if(num==12)
        {
            speed = 1.0f;
        }
        else
        {
            speed = 0.70f;
        }
    }
}
