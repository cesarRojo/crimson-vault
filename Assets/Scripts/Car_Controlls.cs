﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car_Controlls : MonoBehaviour
{
    [Header("Opciones de Cámara:")]
    public Camera cam;
    public Transform centerOfMass;
    public WheelCollider WC_LF;
    public WheelCollider WC_RF;
    public WheelCollider WC_LB;
    public WheelCollider WC_RB;

    public Transform W_LF;
    public Transform W_RF;
    public Transform W_LB;
    public Transform W_RB;

    public float motorTorque = 550f;
    public float maxSteer = 20f;
    private Rigidbody _rigidbody;

    //SONIDO
    [Header("Efecto de sonido: ")]
    public AudioClip drivingClip;
    AudioSource drivingSource;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.centerOfMass = centerOfMass.localPosition;
        Cursor.lockState = CursorLockMode.Locked;

        //Sonido
        drivingSource = GetComponent<AudioSource>();
        drivingSource.clip = drivingClip;
        drivingSource.volume = 0.05f;
    }
    void FixedUpdate()
    {
        WC_LB.motorTorque = Input.GetAxis("Vertical") * -motorTorque;
        WC_RB.motorTorque = Input.GetAxis("Vertical") * -motorTorque;
        WC_LF.steerAngle = Input.GetAxis("Horizontal") * maxSteer;
        WC_RF.steerAngle = Input.GetAxis("Horizontal") * maxSteer;

        //Sound effect for driving
        if (Input.GetButton("Vertical"))
        {
            drivingSource.pitch += 0.05f;

            if(drivingSource.pitch > 1.5f)
            {
                drivingSource.pitch = 1.5f;
            }

        }
        else{
            drivingSource.pitch -= 0.05f;

            if(drivingSource.pitch < 0f)
            {
                drivingSource.pitch = 0f;
            }
        }
    }

    void Update()
    {
        var pos = Vector3.zero;
        var rot = Quaternion.identity;

        WC_LF.GetWorldPose(out pos, out rot);
        W_LF.position = pos;
        W_LF.rotation = rot;

        WC_RF.GetWorldPose(out pos, out rot);
        W_RF.position = pos;
        W_RF.rotation = rot;

        WC_LB.GetWorldPose(out pos, out rot);
        W_LB.position = pos;
        W_LB.rotation = rot;

        WC_RB.GetWorldPose(out pos, out rot);
        W_RB.position = pos;
        W_RB.rotation = rot;
    }
    
}
