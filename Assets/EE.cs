﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EE : MonoBehaviour
{
    public GameObject completeLevelUI;

    void OnTriggerEnter()
    {
        completeLevelUI.SetActive(true);
        
        Invoke("load", 3);
    }

    void load()
    {
        SceneManager.LoadScene("Japan_Level");
    }
}
