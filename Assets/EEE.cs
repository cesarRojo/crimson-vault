﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EEE : MonoBehaviour
{
    public GameObject completeLevelUI;

    void OnTriggerEnter()
    {
        completeLevelUI.SetActive(true);

        Invoke("load", 5);
    }

    void load()
    {
        SceneManager.LoadScene("Japan_Level");
    }
}
