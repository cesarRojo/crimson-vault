﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TouchLogicV2 : MonoBehaviour
{
    public bool webglOnly = false;
    public bool removeInWebglOnly = false;

	bool _buttonEnabled = true;
	public Image buttonImage;

	public bool buttonEnabled {
		set {
            if (buttonImage == null)
                buttonImage = GetComponent<Image>();

			_buttonEnabled = value;

			if(_buttonEnabled){
				buttonImage.color = new Color(1f,1f,1f,1f);
			}
			else {
				buttonImage.color = new Color(0.5f,0.5f,0.5f,0.5f);
			}
		}
		get {
			return _buttonEnabled;
		}
	}

    public KeyCode keyCode = KeyCode.None;
	public string key;
	public string axis;
	public bool positive;
	public float axisThreshold = 0.3f;
	private bool began = false;

	private bool pointerEntered = false;

	public void PointerEnter()
	{
		if (!_buttonEnabled)
			return;

		if (Input.GetMouseButton (0) || Input.touchCount > 0) {
			pointerEntered = true;
		}
	}

	public void PointerExit()
	{
		if (!_buttonEnabled)
			return;

		if (Input.GetMouseButton (0) || Input.touchCount > 0) {
			OnTouchEnded ();
			pointerEntered = false;
			began = false;
		}
	}

	public void PointerClick()
	{

		if (!_buttonEnabled)
			return;

		OnTouchEnded ();
	}

	public virtual void Start()
	{
		if(buttonImage == null)
			buttonImage = GetComponent<Image> ();
	}

	public virtual void Update()//If your child class uses Update, you must call base.Update(); to get this functionality
	{

        if ((webglOnly && !ComicCommon.webVersion) ||
            (removeInWebglOnly && ComicCommon.webVersion)
            )
        {
            buttonEnabled = false;
            transform.SetParent(null);
            //gameObject.SetActive(false);
        }

        

		//Debug.Log ("_buttonEnabled: "+_buttonEnabled);
		if (!_buttonEnabled)
			return;

				if (pointerEntered && (Input.GetMouseButton (0) || Input.touchCount > 0)) {
						if (!began) {
								OnTouchBegan ();
								began = true;
						} else {
								OnTouchStayed ();
						}
				} else {
						pointerEntered = false;
				}

				if (began && Input.GetMouseButtonUp(0)) {
			//todo
				}

				if (!pointerEntered) {
                        if (keyCode != KeyCode.None)
                        {
                            if (Input.GetKeyDown(keyCode))
                            {
                                OnTouchBegan();
                                began = true;
                            }
                            else if (Input.GetKey(keyCode))
                            {
                                OnTouchStayed();
                            }
                            else if (Input.GetKeyUp(keyCode))
                            {
                                OnTouchEnded();
                                began = false;
                            }
                            else if (began)
                            {
                                OnTouchEnded();
                                began = false;
                            }
                        }
						else if (key != null && key.Length > 0) {
								if (Input.GetButtonDown (key)) {
										OnTouchBegan ();
										began = true;
								} else if (Input.GetButton (key)) {
										OnTouchStayed ();
								} else if (Input.GetButtonUp (key)) {
										OnTouchEnded ();
										began = false;
								} else if (began) {
										OnTouchEnded ();
										began = false;
								}
						} else if (axis != null && axis.Length > 0) {
								if ((Input.GetAxis (axis) > axisThreshold && positive) || (Input.GetAxis (axis) < -axisThreshold && !positive)) {
										if (began) {
												OnTouchStayed ();
										} else {
												OnTouchBegan ();
												began = true;
										}
								} else {
										if (began) {
												OnTouchEnded ();
												began = false;
										}
								}
						}
				}
	}
    //the default functions, define what will happen if the child doesn't override these functions
    //public virtual void OnNoTouches(){}
    public virtual void OnTouchBegan() { }
		
	public virtual void OnTouchStayed(){}
	public virtual void OnTouchEnded(){
		
	}
	//public virtual void OnTouchMoved(){}
	/*public virtual void OnTouchBeganAnywhere(){}
	public virtual void OnTouchEndedAnywhere(){}
	public virtual void OnTouchMovedAnywhere(){}
	public virtual void OnTouchStayedAnywhere(){}*/
}
