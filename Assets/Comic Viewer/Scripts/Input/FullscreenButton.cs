﻿using UnityEngine;
using System.Collections;

public class FullscreenButton : TouchLogicV2 
{
    float lastCalled;
    public override void Start()
	{
        
	}

    public override void Update()
	{
		base.Update ();
	}

	public override void OnTouchBegan()
	{
        toState = !Screen.fullScreen;
        Screen.fullScreen = toState;
	}
	
	public override void OnTouchStayed()
	{

	}
    bool toState = false;
	public override void OnTouchEnded()
	{
        
	}

    IEnumerator SwitchFullScreen()
    {
        yield return new WaitForSeconds(0.5f);
        Screen.fullScreen = toState;
    }
}
