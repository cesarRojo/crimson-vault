﻿using UnityEngine;
using System.Collections;

public class ExitButton : TouchLogicV2 
{
    public override void Start()
	{
        /*if (!Global.instance.isTutorial)
            buttonEnabled = false;*/
	}

    public override void Update()
	{
		base.Update ();
	}

	public override void OnTouchBegan()
	{

	}
	
	public override void OnTouchStayed()
	{

	}

	public override void OnTouchEnded()
	{
        if(GameObject.FindObjectOfType<ComicCanvas>().endEvent != null)
            GameObject.FindObjectOfType<ComicCanvas>().endEvent.Invoke();
	}
}
