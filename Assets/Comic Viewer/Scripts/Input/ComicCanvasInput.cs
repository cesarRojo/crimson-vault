﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ComicCanvasInput : MonoBehaviour
{
	void Start () {
	    if(ComicCommon.instance.isAndroidTV) {
            EventSystem eventSystem = GameObject.FindObjectOfType<EventSystem>();
            eventSystem.enabled = false;
        }
	}
}
