﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class TouchGestures {
	public static Vector2 midPoint, curDist, prevDist;
	public static float touchDelta;

	public static Vector2 curTouch, prevTouch, deltaTouch;
    
    public static bool mouseDown = false;

	public static int CheckForDragMove()
	{
		int returnValue = 0;

        if (Input.GetMouseButton(0) && EventSystem.current.currentSelectedGameObject == null)
        {
            curTouch = Input.mousePosition;
            
            if(!mouseDown)
                prevTouch = Input.mousePosition;

            deltaTouch = curTouch - prevTouch;
            touchDelta = deltaTouch.magnitude;
            
            returnValue = 1;
            prevTouch = Input.mousePosition;
            mouseDown = true;
        }
        else
        {
            mouseDown = false;
            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                curTouch = Input.GetTouch(0).position;
                prevTouch = Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition;
                deltaTouch = Input.GetTouch(0).deltaPosition;
                touchDelta = deltaTouch.magnitude;
                returnValue = 1;
            }
            else if (Input.touchCount == 1 && (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled))
            {
                returnValue = 2;
            }
        }

		return returnValue;
	}

	// Following method check multi touch 
	public static int CheckForPinchZoom()
	{
		int touchEvent = 0;

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            touchEvent = 1;
            touchDelta = -20;
            midPoint = Input.mousePosition;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            touchEvent = 1;
            touchDelta = 20;
            midPoint = Input.mousePosition;
        }
        else
        {
            // These lines of code will take the distance between two touches and zoom in - zoom out at middle point between them
            if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                midPoint = new Vector2(((Input.GetTouch(0).position.x + Input.GetTouch(1).position.x) / 2), ((Input.GetTouch(0).position.y + Input.GetTouch(1).position.y) / 2));
                //midPoint = Camera.main.ScreenToWorldPoint(midPoint);

                curDist = Input.GetTouch(0).position - Input.GetTouch(1).position; //current distance between finger touches
                prevDist = ((Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) - (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition)); //difference in previous locations using delta positions
                touchDelta = curDist.magnitude - prevDist.magnitude;
                // Zoom out
                if (touchDelta > 0)
                {

                }
                //Zoom in
                else if (touchDelta < 0)
                {

                }
                touchEvent = 1;
            }
            // On touch end just check whether image is within screen or not
            else if (Input.touchCount == 2 && (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled || Input.GetTouch(1).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Canceled))
            {
                touchEvent = 2;
            }
        }

		return touchEvent;
	}
}
