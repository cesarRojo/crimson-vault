﻿using UnityEngine;
using System.Collections;

public class ToggleAutoButton : TouchLogicV2 
{
    public override void Start()
	{
        /*if (!Global.instance.isTutorial)
            buttonEnabled = false;*/
	}

    public override void Update()
	{
		base.Update ();
	}

	public override void OnTouchBegan()
	{

	}
	
	public override void OnTouchStayed()
	{

	}

	public override void OnTouchEnded()
	{
		GameObject.FindObjectOfType<ComicCanvas> ().autoPlay = !GameObject.FindObjectOfType<ComicCanvas> ().autoPlay;
	}
}
