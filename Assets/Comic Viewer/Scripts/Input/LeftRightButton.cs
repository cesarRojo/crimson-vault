﻿using UnityEngine;
using System.Collections;

public class LeftRightButton : TouchLogicV2 
{
	public bool reversed;
    public bool page;
	private ComicCanvas comicCanvas;

    public override void Start()
	{
		comicCanvas = GameObject.FindObjectOfType<ComicCanvas> ();
	}

    public override void Update()
	{
		base.Update ();
	}

	public override void OnTouchBegan()
	{

	}
	
	public override void OnTouchStayed()
	{

	}

	public override void OnTouchEnded()
	{
		comicCanvas.NavigatePage (reversed, page);

	}
}
