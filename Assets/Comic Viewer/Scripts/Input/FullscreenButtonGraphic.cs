﻿using UnityEngine;
using System.Collections;

public class FullscreenButtonGraphic : MonoBehaviour
{

	void Start()
	{
        if (ComicCommon.osid != 3)
        {
            gameObject.SetActive(false);
            GameObject.FindObjectOfType<FullscreenButton>().gameObject.SetActive(false);
        }
	}
}
