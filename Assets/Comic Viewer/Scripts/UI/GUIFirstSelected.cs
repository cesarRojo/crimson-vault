﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class GUIFirstSelected : MonoBehaviour {

    EventSystem eventSystem;
    GameObject firstSelectedObject = null;

	// Use this for initialization
	void Start () {
        
        eventSystem = GameObject.FindObjectOfType<EventSystem>();
        if (eventSystem != null)
        {
            firstSelectedObject = eventSystem.firstSelectedGameObject;

            if (!ComicCommon.instance.isAndroidTV)
            {
                eventSystem.sendNavigationEvents = false;
                eventSystem.GetComponent<StandaloneInputModule>().forceModuleActive = false;
            }
            
            if (ComicCommon.webVersion)
            {
                eventSystem.sendNavigationEvents = true;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (!ComicCommon.instance.isAndroidTV || eventSystem == null)
        {
            return;
        }

        if (eventSystem == null || !eventSystem.enabled)
            return;
        if (firstSelectedObject == null)
        {
            firstSelectedObject = eventSystem.firstSelectedGameObject;
        }
        else if (firstSelectedObject != null && eventSystem.currentSelectedGameObject == null)
        {
            eventSystem.SetSelectedGameObject(firstSelectedObject);
        }
        else if (firstSelectedObject == null && eventSystem.currentSelectedGameObject == null && eventSystem.firstSelectedGameObject == null)
        {
            foreach(Button button in GameObject.FindObjectsOfType<Button>()) {
                if(button.IsInteractable()) {
                    firstSelectedObject = button.gameObject;
                    eventSystem.SetSelectedGameObject(firstSelectedObject);
                    break;
                }
            }
        }
	}

    void OnLevelWasLoaded(int level)
    {
        firstSelectedObject = null;
        Start();
    }
}
