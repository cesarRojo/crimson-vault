﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIUtilities {
	public static TextAnchor GetRTLAnchor(TextAnchor previousAlignment) {
		TextAnchor newAlignment = previousAlignment;
		switch(previousAlignment) {
		case TextAnchor.LowerCenter:
			break;
		case TextAnchor.LowerLeft:
			newAlignment = TextAnchor.LowerRight;
			break;
		case TextAnchor.LowerRight:
			newAlignment = TextAnchor.LowerLeft;
			break;
		case TextAnchor.MiddleCenter:
			break;
		case TextAnchor.MiddleLeft:
			newAlignment = TextAnchor.MiddleRight;
			break;
		case TextAnchor.MiddleRight:
			newAlignment = TextAnchor.MiddleLeft;
			break;
		case TextAnchor.UpperCenter:
			break;
		case TextAnchor.UpperLeft:
			newAlignment = TextAnchor.UpperRight;
			break;
		case TextAnchor.UpperRight:
			newAlignment = TextAnchor.UpperLeft;
			break;
		}
		return newAlignment;
	}

    public static void OpenLink(string url)
    {
        if (ComicCommon.webVersion)
            Application.ExternalCall("window.OpenNewTab", url);
        else
            Application.OpenURL(url);
    }

	public static void MovePivot(RectTransform rectTransform, Vector2 newPivot){
		Vector2 tranlation = newPivot - rectTransform.pivot;
		tranlation.Scale (rectTransform.rect.size);
		rectTransform.pivot = newPivot;
		Vector2 newScale = rectTransform.localScale - new Vector3(1f,1f);
		tranlation.Scale(newScale);
		rectTransform.anchoredPosition += tranlation;
	}

	public static void CalculateMinMaxTranslation(float offsetX, RectTransform rectTransform, out Vector2 min, out Vector2 max){
		Image image = rectTransform.GetComponent<Image> ();
		RectTransform panelparent = rectTransform.parent.GetComponent<RectTransform> ();
		float imageWidth = image.mainTexture.width;
		float imageHeight = image.mainTexture.height;
		
		float imageAspect = (float)imageWidth/imageHeight;
		float parentAspect = (float)panelparent.rect.width/panelparent.rect.height;
		
		float scaleRatio;
		
		float imageRelativeWidth = panelparent.rect.width, imageRelativeHeight = panelparent.rect.height;
		
		if (imageAspect < parentAspect) {
			imageRelativeWidth = imageRelativeHeight * imageAspect;
			
		} else {
			imageRelativeHeight = imageRelativeWidth / imageAspect;
			
		}

		float minX = -(imageRelativeWidth * rectTransform.localScale.x)*0.5f + offsetX;
		float maxX = (imageRelativeWidth * rectTransform.localScale.x)*0.5f + offsetX;
		
		float minY = -imageRelativeHeight * rectTransform.localScale.y * 0.5f;
		float maxY = imageRelativeHeight * rectTransform.localScale.y * 0.5f;

		min = new Vector2 (minX, minY);
		max = new Vector2 (maxX, maxY);
	}

	public static void MoveAnchorMinMax(RectTransform rectTransform, Vector2 minAnchor, Vector2 maxAnchor){
		float scale = rectTransform.localScale.x;

		float minXOffset = minAnchor.x - rectTransform.anchorMin.x;
		float minYOffset = minAnchor.y - rectTransform.anchorMin.y;

		float maxXOffset = maxAnchor.x - rectTransform.anchorMax.x;
		float maxYOffset = -(maxAnchor.y - rectTransform.anchorMax.y);

		if (Mathf.Abs(minXOffset) < 0.001f)
			minXOffset = 0f;

		if (Mathf.Abs(minYOffset) < 0.001f)
			minYOffset = 0f;

		if (Mathf.Abs(maxXOffset) < 0.001f)
			maxXOffset = 0f;

		if (Mathf.Abs(maxYOffset) < 0.001f)
			maxYOffset = 0f;



		rectTransform.anchorMin = minAnchor;
		rectTransform.anchorMax = maxAnchor;

		rectTransform.offsetMin = rectTransform.offsetMin - new Vector2(minXOffset * Screen.width, minYOffset * Screen.height);
		rectTransform.offsetMax = rectTransform.offsetMax + new Vector2(maxXOffset * Screen.width, maxYOffset * Screen.height);
	}

	public static void CalculateYOffset(Transform transform, ref float anchorYOffset) {
		foreach (Transform child in transform) {
			RectTransform titleRectTransform = child.GetComponent<RectTransform> ();
			if(titleRectTransform != null)
				anchorYOffset += titleRectTransform.anchorMax.y - titleRectTransform.anchorMin.y;
		}
	}
}
