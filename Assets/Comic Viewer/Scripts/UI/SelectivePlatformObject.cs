﻿using UnityEngine;
using System.Collections;

public class SelectivePlatformObject : MonoBehaviour {

    public int[] hidePlatforms;
    public int[] showPlatforms;

	// Use this for initialization
	void Start () {
        if ((hidePlatforms == null || hidePlatforms.Length == 0) && (showPlatforms == null || showPlatforms.Length == 0))
            return;
        bool shouldHide = false;

        if (!(hidePlatforms == null || hidePlatforms.Length == 0))
        {
            foreach (int plaform in hidePlatforms)
            {
                if (ComicCommon.osid == plaform)
                {
                    shouldHide = true;
                }
            }
        }
        else
        {
            shouldHide = true;
            foreach (int plaform in showPlatforms)
            {
                if (ComicCommon.osid == plaform)
                {
                    shouldHide = false;
                }
            }
        }

        if (shouldHide)
        {
            gameObject.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
