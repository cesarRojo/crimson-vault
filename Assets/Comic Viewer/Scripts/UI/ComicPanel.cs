﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ComicPanel : MonoBehaviour {

	bool _update = false;
    //DynamicWebTextureUI dynamicWebTexture;
    public int pageIndex;

	public bool update {
		set {
			_update = value;
			if(value == true)
				calculatedNewPosition = false;
		}
		get
		{
			return _update;
		}
	}

	RectTransform panel;
	RectTransform panelparent;
	Image image;

	public float lerpTime = 5f;
	private float lerpPosition;
	private bool inTransition = false;

	public Vector2 startPoint = new Vector2(0f,0f);
	public Vector2 size = new Vector2(1f,1f);

	public float offsetX;
	public float displayedWidth, displayedHeight;

	public float normalizedDisplayedWidth, normalizedDisplayedHeight;
	public Vector2 normalizedAnchorMax, normalizedAnchorMin, normalizedScale = new Vector2(1f,1f);

	public ComicCanvas comicCanvas;

	private bool calculatedNewPosition = false;
	private Vector2 newAnchorPositon;
	public Vector2 newAnchorMax, newAnchorMin, newScale, highlighterAnchorMax, highlighterAnchorMin;
	public int activeIndex, previousActiveIndex;

	public List<Rect> subPositions;

	public Sprite loadingSprite;

	//public WebTexture webTexture;
	bool activated = false;
	public void AssignReferences()
	{
		panel = GetComponent<RectTransform> ();
		panelparent = transform.parent.GetComponent<RectTransform> ();
		image = GetComponent<Image> ();
		//dynamicWebTexture = GetComponent<DynamicWebTextureUI> ();
		//dynamicWebTexture.Initialize (webTexture, false);
	}

	public void ActivateTexture() {
		activated = true;
		//dynamicWebTexture.SetTexture ();
	}

	public void DeactivateTexture() {
		activated = false;
		//image.sprite = loadingSprite;
		//dynamicWebTexture.DestroyTexture ();
	}

	// Use this for initialization
	void Awake () {
		//AssignReferences ();
	}

	void CalculateNormalizedPosition() {
		if (image == null)
			return;
		float imageWidth = image.mainTexture.width;
		float imageHeight = image.mainTexture.height;
		
		float imageAspect = (float)imageWidth/imageHeight;
		float parentAspect = (float)panelparent.rect.width/panelparent.rect.height;
		
		float scaleRatio;
		
		if(imageAspect < parentAspect)
		{
			scaleRatio = (float)panelparent.rect.height / imageHeight;
			float currentWidth = imageWidth * scaleRatio;
			
			float ratio = 1f - (currentWidth / panelparent.rect.width);
			ratio *= .5f;
			
			normalizedAnchorMax = new Vector2(1f - ratio, 1f);
			normalizedAnchorMin = new Vector2(ratio, 0f);
			
			float newPanelWidth = panelparent.rect.width*(1f-2f*ratio);
			float newPanelHeight = panelparent.rect.height;
			
			normalizedDisplayedWidth = newPanelWidth;
			normalizedDisplayedHeight = newPanelHeight;
		}
		else
		{
			scaleRatio = (float)panelparent.rect.width / imageWidth;
			float currentHeight = imageHeight * scaleRatio;
			
			float ratio = 1f - (currentHeight / panelparent.rect.height);
			ratio *= .5f;
			
			normalizedAnchorMax = new Vector2(1f, 1f - ratio);
			normalizedAnchorMin = new Vector2(0f, ratio);
			
			float newPanelWidth = panelparent.rect.width;
			float newPanelHeight = panelparent.rect.height*(1f-2f*ratio);
			
			normalizedDisplayedWidth = newPanelWidth;
			normalizedDisplayedHeight = newPanelHeight;
		}
	}

	void CalculateNewPosition()
	{
		if (image == null)
			return;

		CalculateNormalizedPosition ();

		float imageWidth = image.mainTexture.width;
		float imageHeight = image.mainTexture.height;

		float imageAspect = (float)imageWidth/imageHeight;
		float imageRelativeAspect = (float)(size.x*imageWidth)/(size.y*imageHeight);
		float parentAspect = (float)panelparent.rect.width/panelparent.rect.height;
		
		float scaleRatio;

		float imageRelativeWidth = panelparent.rect.width, imageRelativeHeight = panelparent.rect.height;
		float relativeX = 1f, relativeY = 1f;
		if (imageAspect < parentAspect) {
			imageRelativeWidth = imageRelativeHeight * imageAspect;
			relativeX = imageRelativeWidth / panelparent.rect.width;
		} else {
			imageRelativeHeight = imageRelativeWidth / imageAspect;
			relativeY = imageRelativeHeight / panelparent.rect.height;
		}

		float x = size.x * relativeX;
		float y = size.y * relativeY;
		Vector2 relativeSize = new Vector2 (x, y);

		x = startPoint.x * relativeX + (1f - relativeX)*0.5f;
		y = startPoint.y * relativeY + (1f - relativeY)*0.5f;
		Vector2 relativeStartPoint = new Vector2 (x, y);

		if(imageRelativeAspect < parentAspect)
		{
			scaleRatio = (float)1f / relativeSize.y;

			highlighterAnchorMax = new Vector2(1f - (1f - relativeSize.x*scaleRatio)*0.5f, 1f);
			highlighterAnchorMin = new Vector2((1f - relativeSize.x*scaleRatio)*0.5f, 0f);
			newAnchorMax = new Vector2(1f, 1f);
			newAnchorMin = new Vector2(0f, 0f);

			displayedWidth = panelparent.rect.width * scaleRatio;
			displayedHeight = panelparent.rect.height * scaleRatio;

			newScale = new Vector2(scaleRatio, scaleRatio);
		}
		else
		{
			scaleRatio = (float)1f / relativeSize.x;

			highlighterAnchorMax = new Vector2(1f, 1f - (1f - relativeSize.y*scaleRatio)*0.5f);
			highlighterAnchorMin = new Vector2(0f, (1f - relativeSize.y*scaleRatio)*0.5f);
			newAnchorMax = new Vector2(1f, 1f);
			newAnchorMin = new Vector2(0f, 0f);

			displayedWidth = panelparent.rect.width * scaleRatio;
			displayedHeight = panelparent.rect.height * scaleRatio;

			newScale = new Vector2(scaleRatio, scaleRatio);
		}
		
		float marginLeft = displayedWidth * (relativeStartPoint.x + relativeSize.x * 0.5f - 0.5f);
		float marginTop = displayedHeight * (relativeStartPoint.y + relativeSize.y * 0.5f - 0.5f);

		newAnchorPositon = new Vector2(-marginLeft + offsetX, marginTop);
		calculatedNewPosition = true;

        comicCanvas.FlashHighLighter();
    }

	void CorrectAnchor()
	{
        if (!calculatedNewPosition)
        {
            CalculateNewPosition();
        }

		lerpPosition += Time.deltaTime/lerpTime;
		
		Vector2 smoothPosition;
		Vector2 smoothAnchorMin;
		Vector2 smoothAnchorMax;
		Vector2 smoothScale;
		if (lerpPosition < 0.9f) {
			smoothPosition = Vector2.Lerp (panel.anchoredPosition, newAnchorPositon, lerpPosition);
			smoothAnchorMin = Vector2.Lerp (panel.anchorMin, newAnchorMin, lerpPosition);
			smoothAnchorMax = Vector2.Lerp (panel.anchorMax, newAnchorMax, lerpPosition);
			smoothScale = Vector2.Lerp(panel.localScale, newScale, lerpPosition);
		}
		else {
            lerpPosition = 0f;
			smoothPosition = newAnchorPositon;
			smoothAnchorMin = newAnchorMin;
			smoothAnchorMax = newAnchorMax;
			smoothScale = newScale;



			update = false;
			inTransition = false;
		}

		panel.anchoredPosition = smoothPosition;
		panel.anchorMin = smoothAnchorMin;
		panel.anchorMax = smoothAnchorMax;
		panel.localScale = smoothScale;
	}

	public void SetActiveIndex(int index, bool force)
	{
		if (index == activeIndex && !force) {
			return;
		}

		calculatedNewPosition = false;
		previousActiveIndex = activeIndex;
		activeIndex = index;
		lerpPosition = 0f;
		update = true;

		startPoint = subPositions [activeIndex].position;
		size = subPositions [activeIndex].size;

        comicCanvas.frameNavigatedEvent.Invoke(pageIndex, activeIndex);
    }

	public bool NavigateIndex(bool reversed)
	{
		int nextIndex = reversed ? activeIndex - 1 : activeIndex + 1;

		if (nextIndex >= subPositions.Count || nextIndex < 0) {
			nextIndex = activeIndex;
			return false;
		}
		
		SetActiveIndex (nextIndex, false);
		return true;
	}

	// Update is called once per frame
	void Update () {
		if (update) {
			if (!Application.isPlaying) {
				Awake();
			}

			CorrectAnchor();
		}
	}
}
