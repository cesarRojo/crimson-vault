﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class ComicCanvas : MonoBehaviour {
    [HideInInspector]
    public List<FrameData> frames = new List<FrameData>();

	public bool autoPlay = true;
	public float autoplayDurationPerFrame = 2.5f;

	float autoPlayWaitDuration = 0f;

	public bool vrMode = false;
	public bool forceOverlayWhenNotInVR = true;

    [Header("Advanced Use Only!")]
    public RectTransform pagesParent;
	public RectTransform displayArea;
    public GameObject pagePrefab;
    public GameObject redHighLighter;
	public CanvasGroup directionButtonsCanvasGroup;

    private RectTransform[] pages;
    private ComicPanel[] comicPanels;

	//public Sprite loadingSprite;

	public bool update
	{
		get {
			return _update;
		}
		set {
			_update = value;
			if(value)
			{
				calculatedNewPosition = false;
			}
		}
	}

    [HideInInspector]
    bool _update;

    [HideInInspector]
    public int activePage;
    [HideInInspector]
    public int previousActivePage;

	

    static ComicCanvas activeInstance;

    public int activeEditPage
    {
        get {
            _activeEditPage = Mathf.Clamp(_activeEditPage, 1, pageSprites.Length);
            return _activeEditPage;
        }

        set
        {
            _activeEditPage = Mathf.Clamp(value, 1, pageSprites.Length);
        }
    }

    int _activeEditPage = 1;

    [Header("Edit Here!")]
    public Sprite[] pageSprites;
    public float lerpTime = 2f;

    
    
	private float lerpPosition;

	private bool hideOtherPages = false;
	private bool inTransition = false;

	private bool calculatedNewPosition = false;
	private Vector2 newAnchorPositon;

	ComicFadableCanvas uiBar;

	int highLighting = 0;

    public FrameNavigatedEvent frameNavigatedEvent;
    public UnityEvent endEvent;

    public int pageCount
    {
        get
        {
            return pageSprites.Length;
        }
    }

    public int currentPageFrameCount
    {
        get
        {
            if (pageCount == 0)
                return 0;
            if (pageCount != frames.Count)
                UpdateFrames();
            return frames[activeEditPage - 1].frameCount;
        }

        set
        {
            if (pageCount == 0)
                return;
            frames[activeEditPage - 1].frameCount = value;
        }
    }

    public List<Rect> currentPageFrame
    {
        get
        {
            return frames[activeEditPage - 1].frames;
        }
    }

    public void UpdateFrames()
    {
        if(frames.Count > pageCount)
            frames.RemoveRange(pageCount, frames.Count - pageCount);

        for (int p = 0; p < pageCount; p++)
        {
            if(p == frames.Count)
                frames.Add(new FrameData());
        }
    }

	void CreatePages()
	{
		for (int i = 0; i < pageCount; i++) {
			GameObject page = GameObject.Instantiate(pagePrefab);

			ComicPanel panel = page.GetComponent<ComicPanel>();
            panel.comicCanvas = this;
            panel.pageIndex = i;
            RectTransform rectTransform = page.GetComponent<RectTransform>();
			Image image = page.GetComponent<Image>();

			page.transform.SetParent(pagesParent.transform);
			page.transform.localPosition = new Vector3 (page.transform.localPosition.x, page.transform.localPosition.y, 0);

			panel.GetComponent<Image>().sprite = pageSprites[i];
            panel.AssignReferences();
			//panel.loadingSprite = loadingSprite;
			panel.subPositions = frames[i].frames;
			rectTransform.offsetMax = new Vector2();
			rectTransform.offsetMin = new Vector2();
		}
	}

	void AssignReferences()
	{
		pages = new RectTransform[pagesParent.transform.childCount];
		comicPanels = new ComicPanel[pagesParent.transform.childCount];
		for (int i = 0; i < pages.Length; i++) {
			pages[i] = pagesParent.transform.GetChild(i).GetComponent<RectTransform>();
			pages[i].pivot = new Vector2(0.5f, 0.5f);
			comicPanels[i] = pagesParent.transform.GetChild(i).GetComponent<ComicPanel>();
		}
	}

	void InitializeOffsets()
	{
		float offset = 0;
		for (int i = 0; i< comicPanels.Length; i++) {
			comicPanels[i].startPoint = new Vector2(0,0);
			comicPanels[i].size = new Vector2(1f,1f);
			comicPanels[i].offsetX = offset;
			comicPanels[i].update = true;
			comicPanels[i].SendMessage("Update");
			
			offset += comicPanels[i].displayedWidth * 1.1f;
		}
	}

	// Use this for initialization
	void Start () {
        Screen.orientation = ScreenOrientation.AutoRotation;

		Canvas canvas = GetComponent<Canvas> ();
		if (vrMode) {
			pagesParent.anchorMin = new Vector2 (0.2f, 0.2f);
			pagesParent.anchorMax = new Vector2 (0.8f, 0.8f);

			displayArea.anchorMin = new Vector2 (0.2f, 0.2f);
			displayArea.anchorMax = new Vector2 (0.8f, 0.8f);

			canvas.renderMode = RenderMode.WorldSpace;
		} else {
			pagesParent.anchorMin = new Vector2 (0f, 0f);
			pagesParent.anchorMax = new Vector2 (1f, 1f);

			displayArea.anchorMin = new Vector2 (0f, 0f);
			displayArea.anchorMax = new Vector2 (1f, 1f);

			if(forceOverlayWhenNotInVR)
				canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		}


        if (ComicCommon.webPlayer)
            Screen.fullScreen = true;

		uiBar = GameObject.FindObjectOfType<ComicFadableCanvas> ();

		CreatePages ();

		AssignReferences ();
		InitializeOffsets ();
		SetActivePage (0, true, true, false);
		FlashHighLighter ();
	}

	void CalculateNewPosition()
	{
        if (pageCount == 0)
            return;
		newAnchorPositon = new Vector2(-comicPanels [activePage].offsetX, 0);
		calculatedNewPosition = true;
	}

	void MoveToPage()
	{
		if (!calculatedNewPosition) {
			CalculateNewPosition ();
		}
		lerpPosition += Time.deltaTime/lerpTime;

		Vector2 smoothPosition;

		if (lerpPosition < 0.9f) {
			smoothPosition = Vector2.Lerp (pagesParent.anchoredPosition, newAnchorPositon, lerpPosition);
		}
		else {
			lerpPosition = 0f;
			smoothPosition = newAnchorPositon;
			update = false;
			inTransition = false;
		}

		pagesParent.offsetMax = smoothPosition;
		pagesParent.offsetMin = smoothPosition;
	}

    public void NavigatePage(bool reversed)
    {
        NavigatePage(reversed, false);
    }

	public void NavigatePage(bool reversed, bool page)
	{
        if (pageCount == 0)
            return;

        bool movePage = page;
        if (!movePage)
        {
            movePage = !comicPanels[activePage].NavigateIndex(reversed);
        }

        if (!movePage)
        {

		}
		else {
			int nextPage = reversed ? activePage - 1 : activePage + 1;

			if (nextPage >= comicPanels.Length) {
                endEvent.Invoke();
                nextPage = 0;
            }

			if(nextPage < 0)
			{
                nextPage = comicPanels.Length - 1;
            }

			if (nextPage >= comicPanels.Length || nextPage < 0) 
				nextPage = activePage;

            bool reset = false;
            bool endFrame = false;

            if (page)
                reset = true;
            else
            {
                reset = true;

                if (reversed)
                    endFrame = true;
            }
            
            SetActivePage (nextPage, false, reset, endFrame);

            pagesParent.localScale = new Vector2(1f, 1f);
            pagesParent.pivot = new Vector2(0.5f, 0.5f);
        }

		update = true;

        if (!Application.isPlaying)
        {
            comicPanels[activePage].update = true;
            comicPanels[activePage].SendMessage("Update");
        }

		//FlashHighLighter ();
	}

	public void FlashHighLighter()
	{
        if (pageCount == 0)
            return;
		RectTransform hightLighterRectTransform = redHighLighter.GetComponent<RectTransform> ();

		if (vrMode) {
			hightLighterRectTransform.anchorMin = comicPanels [activePage].highlighterAnchorMin * 0.6f + new Vector2 (0.2f, 0.2f);
			hightLighterRectTransform.anchorMax = comicPanels [activePage].highlighterAnchorMax * 0.6f + new Vector2 (0.2f, 0.2f);
		} else {
			hightLighterRectTransform.anchorMin = comicPanels[activePage].highlighterAnchorMin;
			hightLighterRectTransform.anchorMax = comicPanels[activePage].highlighterAnchorMax;
		}

		redHighLighter.GetComponent<Image>().CrossFadeColor(new Color(1f,1f,1f,1f), 0.5f, false, true);
		highLighting++;
		StartCoroutine (TurnOffHighLight(highLighting));
	}

	IEnumerator TurnOffHighLight(int pHighLighting)
	{
		yield return new WaitForSeconds (1f);
		if (highLighting != pHighLighting)
			yield break;
		redHighLighter.GetComponent<Image>().CrossFadeColor(new Color(1f,1f,1f,0f), 0.5f, false, true);
	}

	public void SetActivePage(int index, bool force, bool resetPage, bool endFrame)
	{
        if (pageCount == 0)
            return;

        if (index == activePage && !force && !resetPage)
        {
			return;
		}

		comicPanels [index].ActivateTexture ();
        

        if (activePage != index)
        {
            comicPanels[activePage].DeactivateTexture();
        }

		calculatedNewPosition = false;
		previousActivePage = activePage;
		activePage = index;
		lerpPosition = 0f;
		update = true;

        if (resetPage)
        {
            if(!endFrame)
                comicPanels[activePage].SetActiveIndex(0, true);
            else
                comicPanels[activePage].SetActiveIndex(frames[activePage].frameCount - 1, true);
        }
		HideOtherPages ();
	}

	public void HideOtherPages()
	{
		for (int i = 0; i< comicPanels.Length; i++) {
			if(activePage == i)
			{
				comicPanels[i].GetComponent<Image>().CrossFadeColor(new Color(1f,1f,1f,1f), 0.5f, false, true);
			}
			else
			{
				comicPanels[i].GetComponent<Image>().CrossFadeColor(new Color(1f,1f,1f,0f), 0.5f, false, true);
			}
		}
	}

	public void ShowOtherPages()
	{
		for (int i = 0; i< comicPanels.Length; i++) {
			comicPanels[i].GetComponent<Image>().CrossFadeColor(new Color(1f,1f,1f,1f), 0.5f, false, true);
		}
	}
	bool zoomed = false;
	// Update is called once per frame
	void Update () {

		if (autoPlay) {
			if (directionButtonsCanvasGroup.blocksRaycasts) {
				directionButtonsCanvasGroup.alpha = 0f;
				directionButtonsCanvasGroup.blocksRaycasts = false;
				directionButtonsCanvasGroup.interactable = false;
			}

			autoPlayWaitDuration += Time.deltaTime;

			if (autoPlayWaitDuration >= autoplayDurationPerFrame) {
				NavigatePage (false, false);
				autoPlayWaitDuration = 0f;
			}
		} else {
			if (!directionButtonsCanvasGroup.blocksRaycasts) {
				directionButtonsCanvasGroup.alpha = 1f;
				directionButtonsCanvasGroup.blocksRaycasts = true;
				directionButtonsCanvasGroup.interactable = true;
			}
		}

		if (update) {
			if (!Application.isPlaying) {
				Start();
			}
			
			MoveToPage();
		}

		if(autoPlay)
		{
		}

		if(!vrMode)
		{
			if (Input.touchCount > 0 || Input.anyKey) {
				uiBar.FadeIn();
			} else if(!uiBar.FadingIn) {
				uiBar.FadeOut(1f);
			}
		}
		
		int pinchZoomResult = TouchGestures.CheckForPinchZoom ();
		if (pinchZoomResult == 1 && !arrowButtonDown && !autoPlay)
        {
			uiBar.FadeIn();
			comicPanels[activePage].update = false;

			Vector2 newPivot;

			RectTransformUtility.ScreenPointToLocalPointInRectangle (pages[activePage], TouchGestures.midPoint, null, out newPivot);


			newPivot = Vector2.Scale (newPivot, new Vector2(1f/pages[activePage].rect.size.x, 1f/pages[activePage].rect.size.y));
			newPivot += pages[activePage].pivot;

			UIUtilities.MovePivot(pages[activePage], newPivot);
			
			float scale_factor = TouchGestures.touchDelta * 0.05f;
			scale_factor += pages[activePage].localScale.x;
			
			scale_factor = Mathf.Clamp(scale_factor, 0.05f, 11f);
			
			pages[activePage].localScale = new Vector3 (scale_factor, scale_factor, scale_factor);
			zoomed = true;
			//Limit();

		} else if (pinchZoomResult == 2) {
			//Limit();
		} else {
			//Limit();


			int dragMoveResult = TouchGestures.CheckForDragMove ();
			if (dragMoveResult == 1 && !arrowButtonDown && pageCount > 0 && !autoPlay)
            {
				uiBar.FadeIn();
				comicPanels[activePage].update = false;

				float newX = pages[activePage].anchoredPosition.x + TouchGestures.deltaTouch.x*2f;
				float newY = pages[activePage].anchoredPosition.y + TouchGestures.deltaTouch.y*2f;

				pages[activePage].anchoredPosition = new Vector2(newX, newY);

				//comicPanels[activePage].shouldResetOffset = true;

			} else if (dragMoveResult == 2) {
				//Limit();
			} else {
				//Limit();
			}
		}
		Limit ();
	}

	void Limit() {
        if (pageCount == 0)
            return;
		if(pages[activePage].localScale.x < 0.7f)
			pages[activePage].localScale = new Vector3 (0.7f, 0.7f, 0.7f);
		if(pages[activePage].localScale.x > 10f)
			pages[activePage].localScale = new Vector3 (10f, 10f, 10f);
		
		Vector2 minPosition, maxPosition;
		UIUtilities.CalculateMinMaxTranslation(comicPanels[activePage].offsetX, pages[activePage], out minPosition, out maxPosition);

		float newX = Mathf.Clamp(pages[activePage].anchoredPosition.x, minPosition.x, maxPosition.x);
		float newY = Mathf.Clamp(pages[activePage].anchoredPosition.y, minPosition.y, maxPosition.y);

		pages[activePage].anchoredPosition = new Vector2(newX, newY);

		if (float.IsNaN(pages [activePage].offsetMax.x) || float.IsNaN(pages [activePage].offsetMin.x) ||
		    float.IsInfinity(pages [activePage].offsetMax.x) || float.IsInfinity(pages [activePage].offsetMin.x)) {
			
			pages [activePage].offsetMin = new Vector2(0f,pages [activePage].offsetMin.y);
			pages [activePage].offsetMax = new Vector2(0f,pages [activePage].offsetMax.y);
		}
		
		if (float.IsNaN(pages [activePage].offsetMax.y) || float.IsNaN(pages [activePage].offsetMin.y) ||
		    float.IsInfinity(pages [activePage].offsetMax.y) || float.IsInfinity(pages [activePage].offsetMin.y)) {
			
			pages [activePage].offsetMin = new Vector2(pages [activePage].offsetMin.x,0f);
			pages [activePage].offsetMax = new Vector2(pages [activePage].offsetMax.x,0f);
		}
		
		UIUtilities.MovePivot(pages[activePage], new Vector2(0.5f, 0.5f));
	}

    bool arrowButtonDown = false;

    public void ArrowButtonPointerDown()
    {
        arrowButtonDown = true;
    }

    public void ArrowButtonPointerUp()
    {
        arrowButtonDown = false;
    }

	public void PointerExitedPageCanvas()
	{
		if(vrMode)
			uiBar.FadeIn();
	}

	public void PointerEnteredPageCanvas()
	{
		if(vrMode)
			uiBar.FadeOut(0.5f);
	}

    [HideInInspector]
    [System.NonSerialized]
    public float editorStepWidth;

    [HideInInspector]
    [System.NonSerialized]
    public float drawWidth;

    [HideInInspector]
    [System.NonSerialized]
    public float drawHeight;

    void OnDrawGizmosSelected()
    {
        if (Application.isPlaying)
            return;
        UpdateFrames();

        RectTransform canvasRect = GetComponent<RectTransform>();

        editorStepWidth = canvasRect.sizeDelta.x*1.1f;

        float canvasWidth = canvasRect.sizeDelta.x;
        float canvasHeight = canvasRect.sizeDelta.y;
        float canvasAspect = canvasWidth / canvasHeight;

        float globalOffsetX = 0;

        for (int i = 0; i < currentPageFrameCount; i++)
        {
            if (pageSprites[activeEditPage - 1] == null)
                continue;

            globalOffsetX = i * editorStepWidth;

            float textureWidth = pageSprites[activeEditPage - 1].texture.width;
            float textureHeight = pageSprites[activeEditPage - 1].texture.height;
            float textureAspect = textureWidth / textureHeight;

            drawWidth = canvasWidth;
            drawHeight = canvasHeight;

            if (canvasAspect > textureAspect)
            {
                drawWidth = drawHeight * textureAspect;
            }
            else
            {
                drawHeight = drawWidth / textureAspect;
            }

			float offsetx = (canvasWidth - drawWidth) / 2 + globalOffsetX + transform.position.x - canvasRect.sizeDelta.x/2;
			float offsety = (canvasHeight - drawHeight) / 2 + transform.position.y - canvasRect.sizeDelta.y/2;

            Gizmos.color = new Color(Color.blue.r, Color.blue.g, Color.blue.b, 0.5f);

            Gizmos.DrawGUITexture(new Rect(offsetx, drawHeight + offsety, drawWidth, -drawHeight), pageSprites[activeEditPage - 1].texture);

            Vector3 frameSize = new Vector3(currentPageFrame[i].width * drawWidth, -currentPageFrame[i].height * drawHeight, 20);
            Vector3 firstPoint = new Vector3(currentPageFrame[i].x * drawWidth, (1f - currentPageFrame[i].y) * drawHeight, 20); ;
            Vector3 lastPoint = firstPoint + frameSize;

            Gizmos.DrawCube(new Vector3(
                offsetx + (firstPoint.x + lastPoint.x)/2f,
                offsety + (firstPoint.y + lastPoint.y) / 2f,

                10),

                frameSize);
        }
    }
}
