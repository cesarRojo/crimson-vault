﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

[CustomEditor(typeof(ComicCanvas))]
public class ComicCanvasEditor : Editor
{
    ComicCanvas targetCanvas;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        targetCanvas = (ComicCanvas)target;


        int newValue = EditorGUILayout.IntSlider("Active Edit Page", targetCanvas.activeEditPage, 1, targetCanvas.pageCount);
        if(newValue != targetCanvas.activeEditPage)
        {
            targetCanvas.activeEditPage = newValue;
            SceneView.RepaintAll();
        }

        newValue = EditorGUILayout.IntSlider("Frame Count", targetCanvas.currentPageFrameCount, 1, 20);
        if (newValue != targetCanvas.currentPageFrameCount)
        {
            targetCanvas.currentPageFrameCount = newValue;
            SceneView.RepaintAll();
        }
    }

    void OnEnable()
    {
        try {
            targetCanvas = (ComicCanvas)target;
        }
        catch(System.Exception e)
        {

        }
    }

    Vector3 FramePosToScreenPos(Vector3 framePos)
    {
        return new Vector3(framePos.x * targetCanvas.drawWidth - targetCanvas.drawWidth/2, -framePos.y * targetCanvas.drawHeight + targetCanvas.drawHeight/2, targetCanvas.transform.position.z);
    }

    Vector3 ScreenPosToFramePos(Vector3 screenPos)
    {
        return new Vector3((screenPos.x + targetCanvas.drawWidth / 2) / targetCanvas.drawWidth, ((-screenPos.y + targetCanvas.drawHeight / 2) / targetCanvas.drawHeight), 0);
    }

    void OnSceneGUI()
    {
        if (Application.isPlaying)
            return;

        for (int i = 0; i < targetCanvas.currentPageFrameCount; i++) {
            bool changed = false;

            Rect currentFrame = targetCanvas.currentPageFrame[i];

            Vector3 _pos1 = FramePosToScreenPos(new Vector3(currentFrame.x, currentFrame.y, 0));
            Vector3 _pos2 = FramePosToScreenPos(new Vector3(currentFrame.x + currentFrame.width, currentFrame.y + currentFrame.height, 0));

			Vector3 offset = new Vector3(targetCanvas.transform.position.x, targetCanvas.transform.position.y, 0) + new Vector3(i * targetCanvas.editorStepWidth, 0);

            EditorGUI.BeginChangeCheck();
            Vector3 pos1 = Handles.PositionHandle(_pos1 + offset, Quaternion.identity);
            if (EditorGUI.EndChangeCheck())
            {
                changed = true;

                Undo.RecordObject(target, "Edited Frame");
            }

            EditorGUI.BeginChangeCheck();
            Vector3 pos2 = Handles.PositionHandle(_pos2 + offset, Quaternion.identity);
            if (EditorGUI.EndChangeCheck())
            {
                changed = true;

                Undo.RecordObject(target, "Edited Frame");
            }

            if(changed)
            {
                _pos1 = ScreenPosToFramePos(pos1 - offset);
                _pos2 = ScreenPosToFramePos(pos2 - offset);

                float newWidth = Mathf.Abs(_pos1.x - _pos2.x);
                float newHeight = Mathf.Abs(_pos1.y - _pos2.y);

                targetCanvas.currentPageFrame[i] = new Rect(Mathf.Min(_pos1.x, _pos2.x), Mathf.Min(_pos1.y, _pos2.y), newWidth, newHeight);
            }
        }
    }
}
