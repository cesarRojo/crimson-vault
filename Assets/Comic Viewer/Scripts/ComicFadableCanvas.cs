﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ComicFadableCanvas : MonoBehaviour {

	CanvasGroup canvasGroup;

	void Start() {
		canvasGroup = GetComponent<CanvasGroup> ();
	}
	[HideInInspector]
	public bool FadingOut, FadingIn;
	IEnumerator fadingCoroutine;

	public void FadeOut(float constantTime) {
		if (FadingOut)
			return;

		FadingOut = true;
		FadingIn = false;
		if (fadingCoroutine != null) {
			StopCoroutine (fadingCoroutine);
		}

		fadingCoroutine = FadeCoroutine (0f, 2f, constantTime);
		StartCoroutine (fadingCoroutine);
	}

	public void FadeIn() {
		if (FadingIn)
			return;

		FadingOut = false;
		FadingIn = true;
		if (fadingCoroutine != null) {
			StopCoroutine (fadingCoroutine);
		}
		
		fadingCoroutine = FadeCoroutine (1f, 2f, 0f);
		StartCoroutine (fadingCoroutine);
	}

	IEnumerator FadeCoroutine(float targetAlpha, float time, float constantTime) {
		float waitingTime = 0f;
		while(waitingTime < constantTime) {
			waitingTime += Time.deltaTime;
			yield return null;
		}

		float deltaAlpha = Mathf.Abs(canvasGroup.alpha - targetAlpha);
		time = time * deltaAlpha;

		waitingTime = 0f;
		while(Mathf.Abs(canvasGroup.alpha - targetAlpha) > 0.01) {
			float linearRatio = waitingTime/time;
			canvasGroup.alpha = Mathf.Lerp(canvasGroup.alpha, targetAlpha, linearRatio);

			waitingTime += Time.deltaTime;
			yield return null;
		}

		canvasGroup.alpha = targetAlpha;
		if (targetAlpha < 0.01f)
			canvasGroup.interactable = false;
		else
			canvasGroup.interactable = true;

		FadingOut = false;
		FadingIn = false;
	}
}
