﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SampleEventListener : MonoBehaviour {

	// Use this for initialization
	public void EndEvent () {
        Debug.Log("Comic End Event Fired");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // Update is called once per frame
    public void FrameNavigated (int pageIndex, int frameIndex) {
        Debug.Log("Frame Navigated Event Fired With Page Index " + pageIndex + " Frame Index " + frameIndex);
    }
}
