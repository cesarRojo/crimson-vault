using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ComicCommon {

#if UNITY_WEBGL || UNITY_WEBPLAYER
            public static bool webVersion = true;
#else
    public static bool webVersion = false;
#endif


    public static bool webPlayer
    {
        get
        {
#if UNITY_WEBPLAYER
            return true;
#else
            return false;
#endif
        }
    }

    public static int osid
    {
        get
        {
            int temposid = 4;
#if UNITY_IOS
			temposid = 0;
#elif UNITY_ANDROID
			temposid = 1;
#elif UNITY_WSA
            temposid = 2;
#elif UNITY_WEBGL || UNITY_WEBPLAYER
            temposid = 3;
#endif
            return temposid;
        }
    }

    private static ComicCommon _instance;
	
	public static ComicCommon instance {
		get {
			if(_instance == null)
				_instance = new ComicCommon();
			return _instance;
		}
	}

    private int _isAndroidTV = -1;

    public bool isAndroidTV
    {
        get
        {

            if (_isAndroidTV == -1)
            {
                _isAndroidTV = 0;
                if (!Application.isEditor)
                {
#if UNITY_ANDROID
                    AndroidJavaClass unityPlayerJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                    AndroidJavaObject androidActivity = unityPlayerJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
                    AndroidJavaClass contextJavaClass = new AndroidJavaClass("android.content.Context");
                    AndroidJavaObject modeServiceConst = contextJavaClass.GetStatic<AndroidJavaObject>("UI_MODE_SERVICE");
                    AndroidJavaObject uiModeManager = androidActivity.Call<AndroidJavaObject>("getSystemService", modeServiceConst);
                    int currentModeType = uiModeManager.Call<int>("getCurrentModeType");
                    AndroidJavaClass configurationAndroidClass = new AndroidJavaClass("android.content.res.Configuration");
                    int modeTypeTelevisionConst = configurationAndroidClass.GetStatic<int>("UI_MODE_TYPE_TELEVISION");

                    if (modeTypeTelevisionConst == currentModeType)
                    {
                        _isAndroidTV = 1;
                    }
#endif
                }
            }
            return _isAndroidTV == 1;
        }
    }

    
    private ComicCommon() {
        

    }
}
