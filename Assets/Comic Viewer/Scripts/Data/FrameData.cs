﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class FrameData
{
    public int frameCount
    {
        get
        {
            return frames.Count;
        }
        set
        {
            if (value < 1)
                return;
            if (frames.Count > value)
                frames.RemoveRange(value, frames.Count - value);

            for (int n = 0; n < value; n++)
            {
                if (n == frames.Count)
                {
                    float x0 = 0, x1 = 1, y0 = 0, y1 = 1;
                    Add(new Rect(x0, y0, x1, y1));
                }
            }
        }
    }

    public List<Rect> frames = new List<Rect>();

    public FrameData()
    {
        frameCount = 1;
    }

    public void Add(Rect rect)
    {
        frames.Add(rect);
    }
}
