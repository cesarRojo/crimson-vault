﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Lscript : MonoBehaviour
{

    public GameObject completeLevelUI;

    void OnTriggerEnter()
    {
        completeLevelUI.SetActive(true);
        Cursor.visible = true;
        Screen.lockCursor = false;
        Invoke("load", 3);
    }

    void load()
    {

        SceneManager.LoadScene("StartScreen");
    }
}
